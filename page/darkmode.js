
// This code is pure spaghetti... sorry!

function toggleDarkMode() {

  var body = document.getElementById("main")
  var links0 = document.getElementById("links0")
  var links1 = document.getElementById("links1")
  var links2 = document.getElementById("links2")
  var links3 = document.getElementById("links3")
  var toggledarkthemebutton = document.getElementById('toggledarktheme')

  var currentbodyClass = body.className
  var currentlinks0Class = links0.className
  var currentlinks1Class = links1.className
  var currentlinks2Class = links2.className
  var currentlinks3Class = links3.className
  var currentToggleClass = toggledarkthemebutton.className

  body.className = currentbodyClass == "dark" ? "light" : "dark"
  links0.className = currentlinks0Class == "dark" ? "light" : "dark"
  links1.className = currentlinks1Class == "dark" ? "light" : "dark"
  links2.className = currentlinks2Class == "dark" ? "light" : "dark"
  links3.className = currentlinks3Class == "dark" ? "light" : "dark"
  toggledarkthemebutton.className = currentToggleClass == "dark" ? "light" : "dark"
}


document.addEventListener('DOMContentLoaded', function() {
    var link = document.getElementById('toggledarktheme')
    // onClick's logic below:
    link.addEventListener('click', function() {
         toggleDarkMode()
    });
});