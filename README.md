# serenading-stallman
A firefox addon that plays the neckbeard growth supplement that is the Free Software Song and its many versions.

It has a simple menu that replicates most of the css quirks found on stallman.org

# Why?
I only made this since the true OG plugin singing-stallman does not support the latest firefox versions   
I know that the GNU IceCat still supports it, but those ESR releases wont last forever


# Background
You can find out more about the Free Software Song and the licenses of the featured songs on this page:
  https://www.gnu.org/music/free-software-song.html

